$(document).ready(function(){

  //   $('.addRoom').on('click', function(){
  //       $('<div/>', {
		//     'class':'room',
		//     'style':'cursor:pointer;font-weight:bold;',
		//     'html':''
		// }).appendTo('.rooms');
  //   });

	$('.addRoom').on('click', function(){
		$('.room').first().clone(1).appendTo( ".rooms" ).fadeIn();
	});

	// childrensInput

	$('.childrensInput').on('click', function(){
		$('.childrensAge').fadeIn();
	});

	$('.roomDel').on('click', function(){
	    $(this).closest(".room").fadeOut(260);
	    $('.reservation').fadeOut(200);
	    $('.childrensAge').hide();
	});

	$('.searchButton').on('click', function(){
		$('.variants').fadeIn();
	});

	$('.variants .close').on('click', function(){
		$('.variants').fadeOut(200);
		$('.reservation').fadeOut(200);
	});

	// choice

	$('.broBtn').on('mouseenter', function(){
		 $(this).closest(".choice").find('.row').css('background-color', '#fff');
	}).mouseleave(function(){$(this).closest(".choice").find('.row').css('background-color', 'rgb(187, 187, 187)');});


	// $('.broBtn').on('click', function(){
	// 	 $(this).closest(".choice").find('.row').css('background-color', '#fff');
	// });

	// reservation

	$('.broBtn').on('click', function(){
		$('.reservation').fadeIn(200);
	});

	$('.buy').on('click', function(){
		alert('Бронь оплачена');
		$('.variants').fadeOut(200);
		$('.reservation').fadeOut(200);
	});

});